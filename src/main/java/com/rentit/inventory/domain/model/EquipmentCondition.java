package com.rentit.inventory.domain.model;

public enum  EquipmentCondition { SERVICEABLE, UNSERVICEABLE_REPAIRABLE }
