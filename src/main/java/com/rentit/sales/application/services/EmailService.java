package com.rentit.sales.application.services;

import com.rentit.sales.domain.model.PurchaseOrder;

public interface EmailService {

    public void sendEmailNotification(PurchaseOrder po);
}


