package com.rentit.sales.application.services;

import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.EquipmentCondition;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryItem;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.repository.InventoryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.rentit.inventory.domain.repository.PlantReservationRepository;
import com.rentit.sales.domain.model.POExtension;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;
    @Autowired
    PlantReservationRepository plantReservationRepository;
    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    public PurchaseOrder findPurchaseOrder(Long id) {
        return purchaseOrderRepository.getOne(id);
    }

    public PurchaseOrder createPurchaseOrder(Long plantId, LocalDate startDate, LocalDate endDate) {
        PlantInventoryEntry plant = plantInventoryEntryRepository.getOne(plantId);
        PurchaseOrder order = PurchaseOrder.of(
                plant,
                BusinessPeriod.of(startDate, endDate));
        // Validate PO
        purchaseOrderRepository.save(order);
        return order;
    }

    public PurchaseOrder requestPurchaseOrderExtension(Long id, LocalDate endDate) {
        PurchaseOrder order = purchaseOrderRepository.getOne(id);
//        if (order == null)
//            throw new Exception("Purchase order not found");
        order.requestExtension(endDate);
        purchaseOrderRepository.save(order);
        return order;
    }

    public PurchaseOrder allocatePlantToPurchaseOrder(Long id) {
        PurchaseOrder order = purchaseOrderRepository.getOne(id);
        LocalDate startDate = order.getRentalPeriod().getStartDate();
        LocalDate endDate = order.getRentalPeriod().getEndDate();

        List<PlantInventoryItem> items = inventoryRepository.findAvailableItems(
                order.getPlant(), startDate, endDate);
        if (!items.isEmpty()) {
            PlantReservation reservation = new PlantReservation();
            reservation.setPlant(items.get(0));
            reservation.setSchedule(BusinessPeriod.of(startDate, endDate));
            plantReservationRepository.save(reservation);

            order.registerFirstAllocation(reservation);
            // Validate PO
        } else
            order.reject();

        purchaseOrderRepository.save(order);

        return order;
    }

    public PurchaseOrder rejectPurchaseOrder(PurchaseOrder order) {
        order.reject();
        purchaseOrderRepository.save(order);
        return order;
    }

    public PurchaseOrder closePurcheOrder(PurchaseOrder order) {
        order.close();
        purchaseOrderRepository.save(order);
        return order;
    }

    public PurchaseOrder getPurchaseOrder(long id) {
        return purchaseOrderRepository.getOne(id);
    }

    public List<PurchaseOrder> getAllPurchaseOrders() {
        return purchaseOrderRepository.findAll();
    }

    public PurchaseOrder handleReturnedPlant(Long id, boolean isDamaged) {
        PurchaseOrder po = purchaseOrderRepository.getOne(id);
        PlantInventoryItem item = po.getReservations().get(po.getReservations().size() - 1).getPlant();

        if (po == null) {
            throw new RuntimeException("No such Purchase order found");
        } else if (isDamaged) {
            item.setEquipmentCondition(EquipmentCondition.UNSERVICEABLE_REPAIRABLE);
            plantInventoryItemRepository.saveAndFlush(item);

        }
        List<PlantReservation> reservations = inventoryRepository.findReservations(
                item, po.getRentalPeriod().getStartDate(), po.getRentalPeriod()
                        .getStartDate().plusDays(5));


        if (reservations.size() != 0) {
            for (PlantReservation reservation : reservations) {
                if (reservation.getId().equals(po.getReservations().get(po.getReservations().size() - 1).getId())) {
                    continue;
                }

                List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(reservation.getPurchaseOrder().getPlant(),
                        reservation.getSchedule().getStartDate(), reservation.getSchedule().getEndDate());
                checkAvailableReservation(po, reservation, availableItems);

            }
            po.plantOnRepair();
        }
        else{
            po.plantReturned();
        }
        po = purchaseOrderRepository.save(po);
        return po;
    }


    public void checkAvailableReservation(PurchaseOrder po, PlantReservation reservation, List<PlantInventoryItem> availableItems) {
        if (!availableItems.isEmpty()) {
            PlantReservation reservationToSave = new PlantReservation();
            reservationToSave.setPlant(availableItems.get(0));
            reservationToSave.setSchedule(BusinessPeriod.of(reservation.getSchedule().getStartDate(), reservation.getSchedule().getEndDate()));
            reservationToSave.setPurchaseOrder(po);
            reservationToSave = plantReservationRepository.save(reservationToSave);
            PurchaseOrder newPo = reservation.getPurchaseOrder();
            newPo.addReservation(reservationToSave);
            purchaseOrderRepository.save(newPo);
        } else {
            PurchaseOrder newPo = reservation.getPurchaseOrder();
            newPo.reject();
            newPo = purchaseOrderRepository.save(newPo);
        }
    }
}