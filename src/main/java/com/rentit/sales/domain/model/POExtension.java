package com.rentit.sales.domain.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Embeddable
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class POExtension {
    public enum Status {PENDING, ACCEPTED, REJECTED};
    LocalDate endDate;
    @Enumerated(EnumType.STRING)
    Status status;
}
